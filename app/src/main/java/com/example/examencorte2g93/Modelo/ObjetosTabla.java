package com.example.examencorte2g93.Modelo;

import android.provider.BaseColumns;

public class ObjetosTabla implements  BaseColumns{

    public ObjetosTabla(){

    }

    public static abstract class Objetos{

        public static String TABLE_NAME ="Objetos";
        public static String COLUMN_NAME_ID="id";
        public static String COLUMN_NAME_NOMBRE = "nombre";
        public static String COLUMN_NAME_CODIGO = "codigo";
        public static String COLUMN_NAME_MARCA = "marca";
        public static String COLUMN_NAME_PRECIO = "precio";
        public static String COLUMN_NAME_TIPO = "tipo";

        public static String[]Registrar = new String[]{
                Objetos.COLUMN_NAME_CODIGO,
                Objetos.COLUMN_NAME_MARCA,
                Objetos.COLUMN_NAME_TIPO,
                Objetos.COLUMN_NAME_NOMBRE,
                Objetos.COLUMN_NAME_ID,
                Objetos.COLUMN_NAME_PRECIO,
        };

    }

}
