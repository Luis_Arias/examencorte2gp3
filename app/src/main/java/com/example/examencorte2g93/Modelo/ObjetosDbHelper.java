package com.example.examencorte2g93.Modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class ObjetosDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE="TEXT";
    private static final String COMMA_SEP=",";
    private static final String SQL_DELETE_OBJETO = "DROP TABLE IF EXISTS" + ObjetosTabla.Objetos.TABLE_NAME;
    private static final String DATABASE_NAME = "objetos.db";
    private static final int DATABASE_VERSION = 1;
    private static final String SQL_CREATE_OBJETO="CREATE TABLE" +
            ObjetosTabla.Objetos.TABLE_NAME + "(" +
            ObjetosTabla.Objetos.COLUMN_NAME_CODIGO +
            ObjetosTabla.Objetos.COLUMN_NAME_PRECIO +
            ObjetosTabla.Objetos.COLUMN_NAME_NOMBRE +
            ObjetosTabla.Objetos.COLUMN_NAME_TIPO +
            ObjetosTabla.Objetos.COLUMN_NAME_MARCA +
            ObjetosTabla.Objetos.COLUMN_NAME_ID + "INTEGER PRIMARY KEY" + ");";

    public ObjetosDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    db.execSQL(SQL_CREATE_OBJETO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_OBJETO);
        onCreate(db);
    }
}
