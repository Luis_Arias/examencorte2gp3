package com.example.examencorte2g93.Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.examencorte2g93.Objetos;

public abstract class ObjetosDB implements Persistencia, Proyeccion  {

        private Context context;
        private SQLiteDatabase db;
        private ObjetosDbHelper helper;

        public ObjetosDB(Context context, ObjetosDbHelper helper){
                this.context = context;
                this.helper = helper;
        }

        public ObjetosDB(Context context){
                this.context = context;
                this.helper = new ObjetosDbHelper(this.context);
        }

        @Override
        public  void openDataBase(){
                db = helper.getWritableDatabase();
        }

        public void closeDataBase(){
                helper.close();
        }

        public long updateObjetos(Objetos objeto)
        {

                ContentValues values = new ContentValues();
                        values.put(ObjetosTabla.Objetos.COLUMN_NAME_CODIGO, objeto.getCodigo());
                        values.put(ObjetosTabla.Objetos.COLUMN_NAME_ID, objeto.getId());
                        values.put(ObjetosTabla.Objetos.COLUMN_NAME_MARCA, objeto.getMarca());
                        values.put(ObjetosTabla.Objetos.COLUMN_NAME_NOMBRE, objeto.getNombre());
                        values.put(ObjetosTabla.Objetos.COLUMN_NAME_TIPO, objeto.getTipo());
                        values.put(ObjetosTabla.Objetos.COLUMN_NAME_PRECIO, objeto.getPrecio());

                        this.openDataBase();
                        long af = db.update(ObjetosTabla.Objetos.TABLE_NAME, values, ObjetosTabla.Objetos.COLUMN_NAME_ID + "=" + objeto.getId(), null);
                        return af;
        }

        public long insertObjetos(Objetos objeto){
                ContentValues values = new ContentValues();
                values.put(ObjetosTabla.Objetos.COLUMN_NAME_PRECIO, objeto.getPrecio());
                values.put(ObjetosTabla.Objetos.COLUMN_NAME_TIPO, objeto.getTipo());
                values.put(ObjetosTabla.Objetos.COLUMN_NAME_NOMBRE, objeto.getNombre());
                values.put(ObjetosTabla.Objetos.COLUMN_NAME_MARCA, objeto.getMarca());
                values.put(ObjetosTabla.Objetos.COLUMN_NAME_PRECIO, objeto.getPrecio());

                this.openDataBase();
                long saf = db.insert(ObjetosTabla.Objetos.TABLE_NAME, null, values);
                return saf;

        }

}
