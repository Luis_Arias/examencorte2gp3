package com.example.examencorte2g93.Modelo;

import android.database.Cursor;
import com.example.examencorte2g93.Objetos;

import java.util.List;

public interface Proyeccion {

    public Objetos getObjetos(String codigo);
    public  Objetos readObjetos(Cursor cursor);

    public List<Objetos>OObjetos();

}
