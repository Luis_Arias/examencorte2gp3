package com.example.examencorte2g93.Modelo;

import com.example.examencorte2g93.Objetos;

public interface Persistencia {

    public void openDataBase();
    public void deleteObjeto(int id);
    public void closeDatabase();
    public long insertObjeto(Objetos objeto);
    public long updateObjeto(Objetos objeto);

}
