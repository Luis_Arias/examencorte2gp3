package com.example.examencorte2g93;

import java.io.Serializable;

public class Objetos {

    public String codigo;
    public String marca;
    public String nombre;
    public String precio;
    public String tipo;
    public int id;

    public Objetos(String codigo, String nombre, String marca, String precio, String tipo, int id){
        this.id = id;
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
        this.tipo = tipo;
        this.codigo = codigo;
    }

    public int getId(){return id;}
    public void setId(int id){this.id = id;}
    public String getNombre(){return nombre;}
    public void setNombre(String nombre){this.nombre = nombre;}
    public String getMarca(){return marca;}
    public void setMarca(String marca){this.marca = marca;}
    public String getCodigo(){return codigo;}
    public void setCodigo(String codigo){this.codigo = codigo;}
    public String getPrecio(){return  precio;}
    public void setPrecio(String precio){this.precio = precio;}
    public String getTipo(){return tipo;}
    public void setTipo(String tipo){this.tipo = tipo;}

}
